Pour déployer le housekeeping sur WedCMS, il suffit de faire : 

npm run dev

Puis de renommer le fichier index.html dans build en housekeeping.html

Déplacer le fichier housekeeping.html dans le dossier ressources/ de WedCMS

Déplacer le reste des fichiers du dossier build/ dans le dossier ressources/public/housekeeping/ de WedCMS.