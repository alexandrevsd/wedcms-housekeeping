import React from 'react';
import Loading from './components/Loading';
import Main from './components/Main';
import Login from './components/Login';
import './css/sb-admin.min.css';
import './css/custom.css';
import 'bootstrap/dist/js/bootstrap.min';
import {api} from "./API";

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            logged: false
        }
    }

    async componentDidMount() {
        const response = await api.get('/api/auth/checkLogged');
        const res = response.data;
        if(res.user) {
            this.setState({loading: false, logged: true, user: res.user});
        } else {
            this.setState({loading: false});
        }
    }

    hasBeenLoggedOut = () => {
        this.setState({logged: false});
    }

    setLogged = (user) => {
        this.setState({user, logged: true});
    }

    getApp = () => {
        return {
            hasBeenLoggedOut: this.hasBeenLoggedOut,
            setLogged: this.setLogged
        }
    }

    render() {
        return (
            this.state.loading ?
                <Loading/>
                :
                this.state.logged ?
                    <Main app={this.getApp()}/>
                    :
                    <Login app={this.getApp()}/>
        );
    }

}

export default App;
