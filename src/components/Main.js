import React from "react";
import Wrapper from "./main/Wrapper";
import SideBar from "./main/wrapper/SideBar";
import Container from "./main/wrapper/Container";
import TopBar from "./main/wrapper/container/TopBar";
import MainPage from "./main/pages/MainPage";
import UsersPage from "./main/pages/users/UsersPage";
import UserPage from "./main/pages/users/UserPage";
import UserRoomsPage from "./main/pages/users/UserRoomsPage";
import UserHandPage from "./main/pages/users/UserHandPage";
import {Route, Switch} from "react-router-dom";
import VouchersPage from "./main/pages/vouchers/VouchersPage";
import VoucherAddPage from "./main/pages/vouchers/VoucherAddPage";

class Main extends React.Component {

    app;

    constructor(props) {
        super(props);
        this.app = props.app;
    }

    render() {
        return (
            <Wrapper>
                <SideBar/>
                <Container>
                    <TopBar/>
                    <Switch>
                        <Route exact path={'/housekeeping/users'} render={() => <UsersPage app={this.props.app} />} />
                        <Route exact path={'/housekeeping/user/:id'} render={() => <UserPage app={this.props.app} />} />
                        <Route exact path={'/housekeeping/user/:id/rooms'} render={() => <UserRoomsPage app={this.props.app} />} />
                        <Route exact path={'/housekeeping/user/:id/rooms/:page'} render={() => <UserRoomsPage app={this.props.app} />} />
                        <Route exact path={'/housekeeping/user/:id/hand'} render={() => <UserHandPage app={this.props.app} />} />
                        <Route exact path={'/housekeeping/user/:id/hand/:page'} render={() => <UserHandPage app={this.props.app} />} />
                        <Route exact path={'/housekeeping/vouchers'} render={() => <VouchersPage app={this.props.app} />} />
                        <Route exact path={'/housekeeping/vouchers/:page'} render={() => <VouchersPage app={this.props.app} />} />
                        <Route exact path={'/housekeeping/voucher/add'} render={() => <VoucherAddPage app={this.props.app} />} />
                        <Route exact path={'/housekeeping'} render={() => <MainPage app={this.props.app} />} />
                    </Switch>
                </Container>
            </Wrapper>
        );
    }
}

export default Main;