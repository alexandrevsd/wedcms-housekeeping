import React from "react";

const Input = (props) => {
    return <div className="form-group">
        <input type={props.type} name={props.name} className="form-control form-control-user"
               placeholder={props.placeholder} onChange={props.onChange} value={props.value}/>
    </div>;
}

export default Input;