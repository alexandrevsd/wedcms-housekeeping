import React from "react";

const Container = (props) => <div className="container">
    <div className="row justify-content-center">
        <div className="col-xl-10 col-lg-12 col-md-9">
            <div className="card o-hidden border-0 shadow-lg my-5">
                <div className="card-body p-0">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="p-5">
                                {props.children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>;

export default Container;
