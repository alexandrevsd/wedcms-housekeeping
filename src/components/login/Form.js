import React from "react";

const Form = (props) => <>
    <div className="text-center">
        <h1 className="h4 text-gray-900 mb-4">{props.title}</h1>
    </div>
    <form className="user">
        {props.children}
    </form>
</>;

export default Form;