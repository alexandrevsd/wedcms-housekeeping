const SubmitButton = (props) =>
    <button onClick={props.submitForm} disabled={props.disabled} className="btn btn-primary btn-user btn-block">
        {props.text}
    </button>;

export default SubmitButton;