const HalfPage = (props) => <div className="col-lg-6 mb-4">
    {props.children}
</div>;
export default HalfPage;