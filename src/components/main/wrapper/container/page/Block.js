const Block = (props) => <div className="card shadow mb-4 text-white bg-dark border-secondary">
    {props.title && <div className="card-header py-3 bg-dark border-secondary">
        <h6 className="m-0 font-weight-bold">{props.title}{props.button && <button style={{marginLeft:'1em'}} onClick={props.button.onClick} className="btn btn-secondary btn-sm">{props.button.text}</button>}</h6>
    </div>}
    <div className="card-body">
        {props.children}
    </div>
</div>;
export default Block;