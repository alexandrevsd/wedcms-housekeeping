const EntirePage = (props) => <div className="col-lg-12 mb-4">
    {props.children}
</div>;
export default EntirePage;