const LittleBlock = (props) => <div className="col-xl-3 col-md-6 mb-4">
    <div className={"card border-left-" + props.color + " shadow h-100 py-2 bg-secondary"}>
        <div className="card-body">
            <div className="row no-gutters align-items-center">
                <div className="col mr-2">
                    <div className="text-xs font-weight-bold text-gray-200 text-uppercase mb-1">
                        {props.title}
                    </div>
                    <div className="h5 mb-0 font-weight-bold text-gray-200">{props.children}</div>
                </div>
                <div className="col-auto">
                    <i className={"fas fa-" + props.icon + " fa-2x text-" + props.color} />
                </div>
            </div>
        </div>
    </div>
</div>;
export default LittleBlock;