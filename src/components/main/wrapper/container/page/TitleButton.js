const TitleButton = (props) => <a href={props.link} className="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
    <i className={`fas fa-${props.icon} fa-sm text-white-50`}/> {props.text}
</a>;
export default TitleButton;