const Title = (props) => <div className="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 className="h3 mb-0 text-gray-400">{props.title}</h1>
    {props.button}
</div>;
export default Title;