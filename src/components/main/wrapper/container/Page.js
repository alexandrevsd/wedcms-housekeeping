const Page = (props) => <div className="container-fluid">
    {props.children}
</div>
export default Page;