function Container(props) {
    return <div id="content-wrapper" className="d-flex flex-column">
        <div id="content" className="bg-gradient-black">{props.children}</div>
        <footer className="sticky-footer bg-gradient-black">
            <div className="container my-auto">
                <div className="copyright text-center my-auto">
                    <span>Copyright &copy; Your Website 2020</span>
                </div>
            </div>
        </footer>
    </div>;
}
export default Container;