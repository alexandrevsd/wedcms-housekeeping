const Title = (props) => <h6 className="collapse-header">{props.title}</h6>;
export default Title;