import {Link} from "react-router-dom";
const Item = (props) => <Link className="collapse-item" id={props.title} to={"/housekeeping" + props.link}>{props.title}</Link>;
export default Item;