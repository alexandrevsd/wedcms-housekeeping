import appLogoMini from "../../../../images/appLogoMini.png";

const Container = (props) => <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"
                                 id="accordionSidebar">
    <a className="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div className="sidebar-brand-text mx-3">
            <img src={appLogoMini} alt={'WedCMS logo'} title={'WedCMS'}/>
        </div>
    </a>
    <hr className="sidebar-divider my-0"/>
    {props.children}
</ul>
export default Container;