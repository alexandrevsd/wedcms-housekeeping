import {NavLink} from "react-router-dom";


const NavItem = (props) => {
    return (
        <li className="nav-item">
            <NavLink className="nav-link" to={'/housekeeping' + props.link}>
                <i className={"fas fa-fw fa-" + props.icon}/>
                <span>{props.title}</span>
            </NavLink>
        </li>);
}
export default NavItem;