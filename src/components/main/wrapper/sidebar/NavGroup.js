const NavGroup = (props) => {
    return(<li className="nav-item">
        <a className="nav-link collapsed" href={"#" + props.title} data-toggle="collapse" data-target={"#" + props.title}
           aria-expanded="true" aria-controls={props.title}>
            <i className={"fas fa-fw fa-" +  props.icon}/>
            <span>{props.title}</span>
        </a>
        <div id={props.title} className="collapse" data-parent="#accordionSidebar">
            <div className="bg-white py-2 collapse-inner rounded">
                {props.children}
            </div>
        </div>
    </li>)
}

export default NavGroup;