import SideBarContainer from './sidebar/Container';
import NavItem from "./sidebar/NavItem";
import NavGroup from "./sidebar/NavGroup";
import Item from "./sidebar/navgroup/Item";

const SideBar = () => {
    return (
        <SideBarContainer>
            <NavItem title={"Accueil"} link={"/"} icon={"tachometer-alt"} />

            <hr className="sidebar-divider"/>
            <div className="sidebar-heading">
                Kepler
            </div>

            <NavItem title={"Utilisateurs"} link={"/users"} icon={"users"} />
            <NavItem title={"Catalogue"} link={"/catalogue"} icon={"book"} />
            <NavItem title={"Vouchers"} link={"/vouchers"} icon={"gift"} />
            <hr className="sidebar-divider"/>
            <div className="sidebar-heading">
                WedCMS
            </div>
            <NavGroup title={"News"} icon={"newspaper"}>
                <Item title={"Liste"} link={"/news"} />
                <Item title={"Ajouter une news"} link={"/news/add"} />
            </NavGroup>
            <NavGroup title={"Annonces"} icon={"bullhorn"}>
                <Item title={"Liste"} link={"/announcements"} />
                <Item title={"Ajouter une annonce"} link={"/announcements/add"} />
            </NavGroup>
            <NavItem title={"Boutique"} link={"/shop"} icon={"shopping-cart"} />
            <NavItem title={"Centre d'aide"} link={"/help"} icon={"question"} />



            <hr className="sidebar-divider mb-0"/>
            <NavGroup title={"Paramètres"} icon={"cog"}>
                <Item title={"Kepler"} link={"/settings/kepler"} />
                <Item title={"WedCMS"} link={"/settings/wedcms"} />
                <Item title={"Paypal"} link={"/settings/paypal"} />
            </NavGroup>
            <hr className="sidebar-divider d-none d-md-block"/>
            <div className="text-center d-none d-md-inline">
                <button className="rounded-circle border-0" id="sidebarToggle"/>
            </div>
        </SideBarContainer>
    );
}
export default SideBar;