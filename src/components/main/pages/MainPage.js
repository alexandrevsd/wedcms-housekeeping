import Title from "../wrapper/container/page/Title";
import PageContainer from "../wrapper/container/page/Container";
import HalfPage from "../wrapper/container/page/HalfPage";
import Block from "../wrapper/container/page/Block";
import Page from "../wrapper/container/Page";
import React from "react";
import LittleBlock from "../wrapper/container/page/LittleBlock";
import HotelAlert from "./main/HotelAlert";

const MainPage = (props) => {
    return (<Page>
        <Title title={'Accueil'}/>
        <PageContainer>
            <LittleBlock title={"Connectés"} color={"success"} icon={"globe"}>
                50
            </LittleBlock>
            <LittleBlock title={"Inscrits"} color={"warning"} icon={"user-edit"}>
                50
            </LittleBlock>
            <LittleBlock title={"Apparts"} color={"light"} icon={"building"}>
                50
            </LittleBlock>
            <LittleBlock title={"Mobis"} color={"info"} icon={"box-open"}>
                50
            </LittleBlock>
        </PageContainer>
        <PageContainer>
            <HalfPage>
                <HotelAlert app={props.app}/>
                <Block title={'WedCMS'}>
                    <p>Version 1.0</p>
                    <p>
                        Merci à :
                    </p>
                    <ul className="credits-list">
                        <li><b><a href="https://github.com/Quackster" target="_blank" rel="noreferrer">Quackster</a></b>: Pour le serveur Kepler et les archives de développement Habbo</li>
                        <li><b><a href="https://alex-dev.org/archive/" target="_blank" rel="noreferrer">Archives de Quackster</a></b>: Pour les DCRs, les mobis ajoutés et d'autres ressources</li>
                        <li><b><a href="https://github.com/Quackster/Kepler/" target="_blank" rel="noreferrer">Kepler</a></b>: Pour être le meilleur serveur Habbo old school</li>
                        <li><b><a href="http://forum.ragezone.com/f353/website-habbsite-cms-released-249645/" target="_blank" rel="noreferrer">Habbsite CMS</a></b>: Pour l'inspiration de la home page du site</li>
                        <li><b><a href="https://web.archive.org/web/20070105050103/http://www.habbo.fr/" target="_blank" rel="noreferrer">Habbo France 2007</a></b>: Pour avoir été scrapé jusqu'à l'os</li>
                        <li><b><a href="https://startbootstrap.com/theme/sb-admin-2" target="_blank" rel="noreferrer">SB Admin 2</a></b>: Pour être le thème utilisé sur le Housekeeping</li>
                        <li><b><a href="https://discord.com/users/708445578744561764" target="_blank" rel="noreferrer">Valen001</a></b>: Pour avoir aidé à compiler la version 1.33 de Kepler et apporté du soutien</li>
                        <li><b><a href="https://devbest.com/" target="_blank" rel="noreferrer">DevBest</a> & <a href="https://forum.ragezone.com/" target="_blank" rel="noreferrer">Ragezone</a></b>: Pour les ressources de développement et la communauté</li>
                    </ul>
                </Block>
            </HalfPage>
            <HalfPage>
                <Block title={'Commandes dans le jeu'}>
                    <div className="row">
                        <div className="col-lg-6">
                            <p><b>:givebadge</b> <i>{'{NOM_HABBO}  {CODE_BADGE}'}</i><br/>Donnes un badge à un joueur
                            </p>
                            <p><b>:givecredits</b> <i>{'{NOM_HABBO}  {MONTANT}'}</i><br/>Donnes des crédits à un joueur
                            </p>
                            <p><b>:reload</b> <i>{'{COMPOSANT}'}</i><br/>Recharges un composant spécifique (catalogue,
                                shop, items, models, texts ou settings)</p>
                            <p><b>:shutdown</b><br/>Éteints le serveur</p>
                            <p><b>:setprice</b> <i>{'{SALE_CODE} {PRIX}'}</i><br/>Configure le prix d'un item dans le
                                catalogue</p>
                            <p><b>:setconfig</b> <i>{'{PARAMETRE} {VALEUR}'}</i><br/>Configure la valeur d'un paramètre
                                du serveur</p>
                            <p><b>:hotelalert</b> <i>{'{MESSAGE}'}</i><br/>Envoies un message d'alerte à tous les
                                joueurs en ligne</p>
                            <p><b>:ufos</b><br/>Fais apparaître des OVNIs dans un appart</p>
                        </div>
                        <div className="col-lg-6">
                            <p><b>:talk</b> <i>{'{MESSAGE}'}</i><br/>Parle à tous les joueurs</p>
                            <p><b>:bus</b> <i>open</i><br/>Ouvres la porte de l'InfoBus</p>
                            <p><b>:bus</b> <i>close</i><br/>Fermes la porte de l'InfoBus</p>
                            <p><b>:bus</b> <i>question {'{QUESTION}'}</i><br/>Changes la question de l'InfoBus</p>
                            <p><b>:bus</b> <i>option add {'{REPONSE}'}</i><br/>Ajoutes une réponse possible à l'InfoBus
                            </p>
                            <p><b>:bus</b> <i>option remove {'{NUMERO_REPONSE}'}</i><br/>Supprimes une réponse de
                                l'InfoBus</p>
                            <p><b>:bus</b> <i>status</i><br/>Affiches l'état de l'InfoBus</p>
                            <p><b>:bus</b> <i>reset</i><br/>Remets à zéro l'InfoBus</p>
                        </div>
                    </div>
                </Block>
            </HalfPage>
        </PageContainer>
    </Page>);
}

export default MainPage;