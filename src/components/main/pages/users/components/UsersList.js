import Block from "../../../wrapper/container/page/Block";
import {Link} from "react-router-dom";
import React from "react";

const UsersList = (props) => <Block>
    <form>
        <div className="row">
                <input type="text" onChange={props.usersPage.handleSearchChange} className="form-control form-control-sm no-border-bottom" placeholder="Rechercher un Habbo"/>

        </div>
    </form>
    <div className="row">
        <table className="table table-dark table-hover table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {props.usersPage.state.users.map(user => {
                return <tr>
                    <td>{user.id}</td>
                    <td>{user.username}</td>
                    <td className="text-center">
                        <Link id={user.id} to={"/housekeeping/user/" + user.id} className="btn btn-sm btn-outline-warning" style={{width: '49%'}}>Éditer</Link>
                    </td>
                </tr>
            })}
            </tbody>
        </table>
        <p className="white-link text-center">
            {props.usersPage.state.page > 0 ?
                <a onClick={props.usersPage.handlePreviousPage} href="#previous">{'<< Page précédente'}</a> : '<< Page précédente'} | {(props.usersPage.state.count - (props.usersPage.state.page * props.usersPage.limit)) > props.usersPage.limit ?
            <a onClick={props.usersPage.handleNextPage} href="#next">{'Page suivante >>'}</a> : 'Page suivante >>'}
        </p>
    </div>
</Block>;
export default UsersList;