import React from 'react';
import Block from "../../wrapper/container/page/Block";
import {Link, withRouter} from "react-router-dom";
import {api} from "../../../../API";
import Page from "../../wrapper/container/Page";
import PageContainer from "../../wrapper/container/page/Container";
import Title from "../../wrapper/container/page/Title";
import Alert from "../../../Alert";
import EntirePage from "../../wrapper/container/page/EntirePage";

class UserRoomsPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            page: this.props.match.params.page || 0,
            user: undefined,
            rooms: undefined,
            nextPage: false,
            previousPage: false
        };
    }

    async componentDidMount() {
        await this.loadUser();
        await this.loadRooms();
    }

    async componentDidUpdate(prevProps, prevState) {
        if(prevProps.match.params.page !== this.props.match.params.page) {
            // When page in url changes
            this.setState({page: parseInt(this.props.match.params.page)});
            // Set the new page in state
        }
        if(prevState.page !== this.state.page) {
            // When page in state changes
            this.loadRooms();
            // Reloads rooms with the new page
        }
    }

    loadUser = async () => {
        const response = await api.get('/api/users/user?id=' + this.state.id);
        const res = response.data;
        if(res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            if(res.user) {
                this.setState({user: res.user});
            }
        }
    }

    loadRooms = async () => {
        const response = await api.get('/api/users/rooms?page=' + this.state.page + '&id=' + this.state.id);
        const res = response.data;
        if(res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            if(res.rooms) {
                this.setState({rooms: res.rooms, page: res.page, nextPage: res.nextPage, previousPage: res.previousPage});
            }
        }
    }

    handleButtonDelete = async (e) => {
        e.currentTarget.disabled = true;
        const response = await api.delete('/api/users/room?id=' + e.currentTarget.id);
        const res = response.data;
        if(res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            if(res.room) {
                const rooms = [];
                const oldRooms = [...this.state.rooms];
                for (const room in rooms) {
                    if(room.id !== res.room.id) {
                        rooms.push(room);
                    }
                }
                this.setState({rooms});
            } else {
                e.currentTarget.disabled = false;
            }
        }
    }

    render() {
        return (
            <Page>
                <PageContainer>
                    <Title title={this.state.user ? 'Appartements de ' + this.state.user.username : 'Chargement...'} button={<Link to={'/housekeeping/user/' + this.state.id} className="btn btn-secondary">Retour</Link>}/>
                    {this.state.alert && <Alert color={this.state.alert.color}>{this.state.alert.message}</Alert>}
                    <EntirePage>
                        {this.state.rooms === undefined ?
                            <Block>
                                Chargement en cours...
                            </Block>
                        :
                            <Block>
                                <table className="table table-dark">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nom</th>
                                        <th>Description</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.rooms.map((room, i) => {
                                            return <tr key={i}>
                                                <td>{room.id}</td>
                                                <td>{room.name}</td>
                                                <td>{room.description}</td>
                                                <td style={{textAlign:'center'}}><button onClick={this.handleButtonDelete} id={room.id} className="btn btn-sm btn-outline-danger"><i className={"fas fa-fw fa-trash"}/></button></td>
                                            </tr>
                                        })}
                                    <tr>
                                        
                                    </tr>
                                    </tbody>
                                </table>
                                <p className="mb-2 text-center white-link">
                                    {this.state.previousPage ? <Link to={'/housekeeping/user/' + this.state.id + '/rooms/' + (this.state.page - 1)} disabled>{'<< Page précédente'}</Link> : '<< Page précédente'} | {this.state.nextPage ? <Link to={'/housekeeping/user/' + this.state.id + '/rooms/' + (this.state.page + 1)}>{'Page suivante >>'}</Link> : 'Page suivante >>'}
                                    </p>
                            </Block>
                        }
                    </EntirePage>
                </PageContainer>
            </Page>
        );
    }   
}

export default withRouter(UserRoomsPage);