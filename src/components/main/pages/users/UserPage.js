import React from 'react';
import Block from "../../wrapper/container/page/Block";
import {Link, withRouter} from "react-router-dom";
import {api} from "../../../../API";
import Page from "../../wrapper/container/Page";
import PageContainer from "../../wrapper/container/page/Container";
import Title from "../../wrapper/container/page/Title";
import Alert from "../../../Alert";
import EntirePage from "../../wrapper/container/page/EntirePage";

class UserPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: undefined,
            inputUsername: undefined,
            inputPassword: undefined,
            inputCredits: undefined,
            inputSex: undefined
        };
    }

    async componentDidMount() {
        await this.loadUser();
    }

    loadUser = async () => {
        const id = this.props.match.params.id;
        const response = await api.get('/api/users/user?id=' + id);
        const res = response.data;
        if(res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            if(res.user) {
                const user = {
                    ...res.user
                }
                this.setState({user, inputUsername: user.username, inputCredits: user.credits, inputSex: user.sex});
            }
        }
    }

    handleInputUsername = (e) => this.setState({inputUsername: e.target.value});
    handleInputPassword = (e) => this.setState({inputPassword: e.target.value});
    handleInputSex = (e) => this.setState({ inputSex: e.target.value });
    handleInputCredits = (e) => this.setState({inputCredits: e.target.value});

    handleSubmitUser = async (e) => {
        e.preventDefault();
        const user = {
            id: this.props.match.params.id,
            username: this.state.inputUsername,
            password: this.state.inputPassword,
            credits: this.state.inputCredits,
            sex: this.state.inputSex
        }
        const response = await api.post('/api/users/user', {user})
        const res = response.data;
        if(res.user) {
            
        } else {

        }
        
    }

    render() {
        return (
            <Page>
                <PageContainer>
                    <Title title={this.state.user ? 'Profil de ' + this.state.user.username : 'Chargement...'} 
                    button={<div className="btn-toolbar mb-2 mb-md-0">
                        <Link to={'/housekeeping/user/' + this.props.match.params.id + '/rooms'} className="btn btn-outline-warning mr-2">
                            Appartements
                        </Link>
                        <Link to={'/housekeeping/user/' + this.props.match.params.id + '/hand'} className="btn btn-outline-warning mr-2">
                            Main
                        </Link>
                        <Link to={'/housekeeping/users'} className="btn btn-secondary">Retour</Link>
                    </div>}/>
                    {this.state.alert && <Alert color={this.state.alert.color}>{this.state.alert.message}</Alert>}
                    <EntirePage>
                        {this.state.user === undefined ?
                            <Block>
                                Chargement en cours...
                            </Block>
                        :
                            <Block>
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="name">Nom Habbo</label>
                                        <input type="text" id="username" className="form-control form-control-sm" onChange={this.handleInputUsername} value={this.state.inputUsername} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="password">Mot de passe</label>
                                        <input type="password" className="form-control form-control-sm" id="password" onChange={this.handleInputPassword} value={this.state.inputPassword} aria-describedby="passwordHelp" />
                                        <small id="passwordHelp" className="form-text text-muted">Laisser vide pour ne pas changer.</small>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="password">Sexe</label>
                                        <select onChange={this.handleInputSex} defaultValue={this.state.user.sex} className="form-control form-control-sm">
                                            <option value="M">Garçon</option>
                                            <option value="F">Fille</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="credits">Crédits</label>
                                        <input type="number" id="credits" step="1" onChange={this.handleInputCredits} value={this.state.inputCredits} className="form-control form-control-sm" />
                                    </div>
                                    <button onClick={this.handleSubmitUser} className="btn btn-primary btn-sm mb-2" style={{width:"100%"}}>Sauvegarder</button>
                                </form>
                            </Block>
                        }
                    </EntirePage>
                </PageContainer>
            </Page>
        );
    }   
}

export default withRouter(UserPage);