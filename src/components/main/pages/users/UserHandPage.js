import React from 'react';
import Block from "../../wrapper/container/page/Block";
import {Link, withRouter, useSearchParams} from "react-router-dom";
import {api} from "../../../../API";
import Page from "../../wrapper/container/Page";
import PageContainer from "../../wrapper/container/page/Container";
import Title from "../../wrapper/container/page/Title";
import Alert from "../../../Alert";
import EntirePage from "../../wrapper/container/page/EntirePage";

class UserHandPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            page: this.props.match.params.page || 0,
            user: undefined,
            items: undefined,
            furnis: undefined,
            nextPage: false,
            previousPage: false
        };
    }

    async componentDidMount() {
        await this.loadUser();
        await this.loadHand();
    }

    async componentDidUpdate(prevProps, prevState) {
        if(prevProps.match.params.page !== this.props.match.params.page) {
            // When page in url changes
            this.setState({page: parseInt(this.props.match.params.page)});
            // Set the new page in state
        }
        if(prevState.page !== this.state.page) {
            // When page in state changes
            this.loadHand();
            // Reloads hand with the new page
        }
    }

    loadUser = async () => {
        const response = await api.get('/api/users/user?id=' + this.state.id);
        const res = response.data;
        if(res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            if(res.user) {
                this.setState({user: res.user});
            }
        }
    }

    loadHand = async () => {
        const response = await api.get('/api/users/hand?page=' + this.state.page + '&id=' + this.state.id);
        const res = response.data;
        if(res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            if(res.items && res.furnis) {
                this.setState({page: parseInt(res.page), items: res.items, furnis: res.furnis, nextPage: res.nextPage, previousPage: res.previousPage});
            }
        }
    }

    handleButtonDelete = async (e) => {
        e.currentTarget.disabled = true;
        const response = await api.delete('/api/users/item?id=' + e.currentTarget.id);
        const res = response.data;
        if(res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            if(res.item && res.furni) {
                let definitionToDelete = undefined;
                const items = [];
                const oldItems = [...this.state.items];
                for(let i = 0; i < oldItems.length; i++) {
                    if(oldItems[i].id !== res.item.id) {
                        items.push(oldItems[i]);
                    } else {
                        definitionToDelete = oldItems[i].defintion_id;
                    }
                }
                const furnis = [];
                const oldFurnis = [...this.state.furnis];
                for(let i = 0; i < oldFurnis.length; i++) {
                    if(oldFurnis[i].id !== res.item.id) {
                        furnis.push(oldFurnis[i]);
                    }
                }
                this.setState({items, furnis});
            } else {
                e.currentTarget.disabled = false;
            }
        }
    }

    handleButtonEmpty = async (e) => {
        e.currentTarget.disabled = true;
        const response = await api.delete('/api/users/hand?id=' + this.state.user.id);
        const res = response.data;
        if(res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            if(res.response === 200) {
                const items = [], furnis = [];
                this.setState({items, furnis});
            } else {
                e.currentTarget.disabled = false;
            }
        }
    }

    render() {
        return (
            <Page>
                <PageContainer>
                    <Title title={this.state.user ? 'Main de ' + this.state.user.username : 'Chargement...'} button={<Link to={'/housekeeping/user/' + this.state.id} className="btn btn-secondary">Retour</Link>}/>
                    {this.state.alert && <Alert color={this.state.alert.color}>{this.state.alert.message}</Alert>}
                    <EntirePage>
                        {this.state.items === undefined || this.state.furnis === undefined || this.state.user === undefined ?
                            <Block>
                                Chargement en cours...
                            </Block>
                        :
                            <Block>
                                <table className="table table-dark">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nom du mobis</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.items.map((item, i) => {
                                            const furnis = this.state.furnis;
                                            return <tr key={i}>
                                                <td>{item.id}</td>
                                                <td>{furnis[i].name}</td>
                                                <td style={{textAlign:'center'}}><button id={item.id} onClick={this.handleButtonDelete} className="btn btn-sm btn-outline-danger"><i className={"fas fa-fw fa-trash"}/></button></td>
                                            </tr>
                                        })}
                                    <tr>
                                        
                                    </tr>
                                    </tbody>
                                </table>
                                <p className="mb-2 text-center white-link">
                                    {this.state.previousPage ? <Link to={'/housekeeping/user/' + this.state.id + '/hand/' + (this.state.page - 1)} disabled>{'<< Page précédente'}</Link> : '<< Page précédente'} | {this.state.nextPage ? <Link to={'/housekeeping/user/' + this.state.id + '/hand/' + (this.state.page + 1)}>{'Page suivante >>'}</Link> : 'Page suivante >>'}
                                    </p>
                                <button onClick={this.handleButtonEmpty} className="btn btn-outline-danger btn-sm" style={{width:"100%"}}>Vider la main</button>
                            </Block>
                        }
                    </EntirePage>
                </PageContainer>
            </Page>
        );
    }   
}

export default withRouter(UserHandPage);