import React from 'react';
import Title from "../../wrapper/container/page/Title";
import Page from "../../wrapper/container/Page";
import PageContainer from "../../wrapper/container/page/Container";
import UsersList from "./components/UsersList";
import EntirePage from "../../wrapper/container/page/EntirePage";
import Alert from "../../../Alert";
import {api} from "../../../../API";

class UsersPage extends React.Component {

    limit = 10;

    constructor(props) {
        super(props);
        this.state = {
            users: [],
            count: 0,
            page: 0,
            search: ''
        };
    }

    async componentDidMount() {
        await this.reloadList();
    }

    reloadList = async () => {
        const response = await api.post('/api/users/list', {
            page: this.state.page,
            limit: this.limit,
            search: this.state.search
        });
        const res = response.data;
        if (res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            this.setState({users: res.users, count: res.count});
        }
    }

    handlePreviousPage = async (e) => {
        e.preventDefault();
        const page = this.state.page - 1;
        this.setState({page});
        await this.reloadList();
    }

    handleNextPage = async (e) => {
        e.preventDefault();
        const page = this.state.page + 1;
        this.setState({page});
        await this.reloadList();
    }

    handleSearchChange = async (e) => {
        this.setState({search: e.target.value});
        await this.reloadList();
    }

    getUsersPage = () => {
        return {
            limit: this.limit,
            state: this.state,
            handleSearchChange: this.handleSearchChange,
            handleNextPage: this.handleNextPage,
            handlePreviousPage: this.handlePreviousPage
        }
    }

    render() {
        return (
            <Page>
                <PageContainer>
                    <Title title={'Utilisateurs'}/>
                    {this.state.alert && <Alert color={this.state.alert.color}>{this.state.alert.message}</Alert>}
                    <EntirePage>
                        <UsersList usersPage={this.getUsersPage()}/>
                    </EntirePage>
                </PageContainer>
            </Page>
        );
    }


}

export default UsersPage;