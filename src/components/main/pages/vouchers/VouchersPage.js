import React from 'react';
import {Link, withRouter} from "react-router-dom";
import Title from "../../wrapper/container/page/Title";
import Page from "../../wrapper/container/Page";
import PageContainer from "../../wrapper/container/page/Container";
import EntirePage from "../../wrapper/container/page/EntirePage";
import Block from "../../wrapper/container/page/Block";
import Alert from "../../../Alert";
import {api} from "../../../../API";

class VouchersPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            page: this.props.match.params.page || 0,
            vouchers: undefined
        };
    }

    async componentDidMount() {
        await this.loadVouchers();
    }

    loadVouchers = async () => {
        const response = await api.get('/api/vouchers?page=' + this.state.page);
        const res = response.data;
        if (res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            if(res.vouchers) {
                this.setState({vouchers: res.vouchers});
            }
        }
    }

    handleButtonDelete = async (e) => {
        e.currentTarget.disabled = true;
        const response = await api.delete('/api/voucher?voucher_code=' + e.currentTarget.id);
        const res = response.data;
        if(res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            if(res.voucher) {
                const vouchers = [];
                const oldVouchers = [...this.state.vouchers];
                for(let i = 0; i < oldVouchers.length; i++) {
                    if(oldVouchers[i].voucher_code !== res.voucher.voucher_code) {
                        vouchers.push(oldVouchers[i]);
                    }
                }
                this.setState({vouchers});
            } else {
                e.currentTarget.disabled = false;
            }
        }
    }

    render() {
        return (
            <Page>
                <PageContainer>
                    <Title title={'Vouchers'}
                    button={<div className="btn-toolbar mb-2 mb-md-0">
                    <Link to={'/housekeeping/voucher/add'} className="btn btn-outline-success">
                        Nouveau
                    </Link>
                    </div>}/>
                    {this.state.alert && <Alert color={this.state.alert.color}>{this.state.alert.message}</Alert>}
                    <EntirePage>
                        <Block>
                            {this.state.vouchers === undefined ?
                                <h2 style={{textAlign:'center'}}>Aucun vouchers dans la base de données</h2>
                            :
                                <div className="row">
                                    <table className="table table-dark table-hover table-striped">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Crédits</th>
                                            <th>Mobis</th>
                                            <th>Expiration</th>
                                            <th>Unique</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {this.state.vouchers.map(voucher => {
                                            const date = new Date(voucher.expiry_date);
                                            return <tr key={voucher.voucher_code}>
                                                <td>{voucher.voucher_code}</td>
                                                <td>{voucher.credits}</td>
                                                <td>{voucher.furnis}</td>
                                                <td>{date.toISOString().slice(0, 19).replace('T', ' ')}</td>
                                                <td>{voucher.is_single_use ? 'Oui' : 'Non'}</td>
                                                <td className={'text-center'}><button onClick={this.handleButtonDelete} id={voucher.voucher_code} className="btn btn-sm btn-outline-danger"><i className={"fas fa-fw fa-trash"}/></button></td>
                                            </tr>
                                        })}
                                        </tbody>
                                    </table>
                                </div>
                            }
                        </Block>
                    </EntirePage>
                </PageContainer>
            </Page>
        );
    }


}

export default withRouter(VouchersPage);