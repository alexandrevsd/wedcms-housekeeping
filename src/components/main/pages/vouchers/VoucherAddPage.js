import React from 'react';
import {Link, withRouter} from "react-router-dom";
import Title from "../../wrapper/container/page/Title";
import Page from "../../wrapper/container/Page";
import PageContainer from "../../wrapper/container/page/Container";
import EntirePage from "../../wrapper/container/page/EntirePage";
import Block from "../../wrapper/container/page/Block";
import Alert from "../../../Alert";
import {api} from "../../../../API";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import fr from 'date-fns/locale/fr';
registerLocale('fr', fr);

class VoucherAddPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            inputFurnis: [],
            inputCode: undefined,
            inputCredits: 0,
            inputExpiryDate: new Date(),
            inputIsSingleUse: true,
            inputFurniToAdd: {value: undefined, text: undefined}
        };
    }

    async componentDidMount() {
        await this.loadItems();
    }

    loadItems = async () => {
        const response = await api.get('/api/catalogue/items');
        const res = response.data;
        if (res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            if(res.items) {
                this.setState({items: res.items});
            }
        }
    }

    handleInputCode = (e) => this.setState({inputCode: e.target.value});
    handleInputCredits = (e) => this.setState({inputCredits: e.target.value});
    handleInputExpiryDate = (date) => this.setState({inputExpiryDate: date});
    handleInputIsSingleUse = (e) => this.setState({inputIsSingleUse: e.target.checked});
    handleInputFurniToAdd = (e) => this.setState({inputFurniToAdd: {value: e.target.value, text: e.target.options[e.target.selectedIndex].text}});
    handleInputFurniToDelete = (e) => this.setState({inputFurniToDelete: {value: e.target.value, text: e.target.options[e.target.selectedIndex].text}});

    handleButtonAddItem = (e) => {
        e.preventDefault();
        const furni = {
            sale_code: this.state.inputFurniToAdd.value,
            name: this.state.inputFurniToAdd.text
        }
        const inputFurnis = [furni, ...this.state.inputFurnis];
        this.setState({inputFurnis});
    }

    handleButtonDeleteItem = (e) => {
        e.preventDefault();
        const inputFurnis = [];
        for(let i = 0; i < this.state.inputFurnis.length; i++) {
            if(this.state.inputFurnis[i].sale_code !== this.state.inputFurniToDelete.value) {
                inputFurnis.push(this.state.inputFurnis[i]);
            }
        }
        this.setState({inputFurnis});
    }

    handleSubmitVoucher = async (e) => {
        e.preventDefault();
        const voucher = {
            voucher_code: this.state.inputCode,
            credits: this.state.inputCredits,
            expiry_date: this.state.inputExpiryDate,
            is_single_use: this.state.inputIsSingleUse,
            items: this.state.inputFurnis
        }
        const response = await api.post('/api/voucher', {voucher})
        const res = response.data;
    }

    render() {
        return (
            <Page>
                <PageContainer>
                    <Title title={'Nouveau bon d\'achat'}
                    button={<div className="btn-toolbar mb-2 mb-md-0">
                    <Link to={'/housekeeping/vouchers'} className="btn btn-outline-danger">
                        Annuler
                    </Link>
                    </div>}/>
                    {this.state.alert && <Alert color={this.state.alert.color}>{this.state.alert.message}</Alert>}
                    <EntirePage>
                        <Block>
                            <form>
                                <div className="form-group">
                                    <label htmlFor="name">Code du bon d'achat</label>
                                    <input onChange={this.handleInputCode} value={this.state.inputCode} type="text" id="voucher_code" className="form-control form-control-sm" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="name">Crédits à donner</label>
                                    <input onChange={this.handleInputCredits} value={this.state.inputCredits} type="number" id="credits" className="form-control form-control-sm" />
                                </div>
                                <div className="form-group" style={{marginBottom:"0em"}}>
                                    <label htmlFor="name">Mobis à donner</label>
                                </div>
                                <div className="input-group mb-3">
                                    <select onChange={this.handleInputFurniToDelete} selected={this.state.inputFurniToDelete} multiple className="form-select">
                                        {this.state.inputFurnis.map((furni) => {
                                            return <option key={furni.sale_code} value={furni.sale_code}>
                                                {furni.name}
                                            </option>
                                        })}
                                    </select>
                                    <div style={{display:'flex',flexDirection:'column',justifyContent:'space-around',margin:'0 1em 0 1em'}}>
                                        <button onClick={this.handleButtonAddItem} className="btn btn-success">{'<<'}</button>
                                        <button onClick={this.handleButtonDeleteItem} className="btn btn-danger">Supprimer</button>
                                    </div>
                                    <select onChange={this.handleInputFurniToAdd} selected={this.state.inputFurniToAdd.value} multiple className="form-select">
                                        {this.state.items.map((item) => {
                                            return <option key={item.id} value={item.sale_code}>
                                                {item.package_name !== null ? item.package_name : item.name}
                                            </option>
                                        })}
                                    </select>
                                </div>
                                <div className="form-group" style={{marginLeft: "1.3em"}}>
                                    <input onChange={this.handleInputIsSingleUse} checked={this.state.inputIsSingleUse} type="checkbox" id="isSingleUse" className="form-check-input" ></input>
                                    <label className="form-check-label" htmlFor="isSingleUse">
                                        Usage unique
                                    </label>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="name">Expiration du bon d'achat</label>
                                    <DatePicker
                                        selected={this.state.inputExpiryDate}
                                        onChange={(date) => this.handleInputExpiryDate(date)}
                                        showTimeSelect
                                        timeFormat="HH:mm"
                                        locale="fr"
                                        className="form-control form-control-sm"
                                        dateFormat="dd/MM/yy HH:mm"
                                    />
                                </div>
                                <button onClick={this.handleSubmitVoucher} type="submit" className="btn btn-success btn-sm mb-2" style={{width:"100%"}}>Créer le bon d'achat</button>
                            </form>
                        </Block>
                    </EntirePage>
                </PageContainer>
            </Page>
        );
    }


}

export default withRouter(VoucherAddPage);