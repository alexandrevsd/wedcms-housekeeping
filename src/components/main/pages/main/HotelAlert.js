import React from "react";
import Block from "../../wrapper/container/page/Block";
import {api} from "../../../../API";

class HotelAlert extends React.Component {

    constructor(props) {
        super(props);
        this.state = {inputMessage: '', disabledButton: false}
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        this.setState({disabledButton: true});
        const response = await api.post('/api/rcon/sendHotelAlert', {message: this.state.inputMessage});
        const res = response.data;
        console.log(res)
        if (res.logged !== undefined && res.logged === false) {
            this.props.app.hasBeenLoggedOut();
        } else {
            if (!res.error) {
                this.setState({inputMessage: ''});
                alert('Alerte envoyée !');
            } else {
                if (res.error === 'noMessage')
                    alert('Le message envoyé est vide !');
            }
        }
        this.setState({disabledButton: false});
    }

    render() {
        return (
            <Block title={'Alerte hôtel'}>
                <form>
                    <div className="form-group">
                        <input type="text" id="alertMessage" className="form-control form-control-sm"
                               placeholder="Message de l'alerte" onChange={this.handleMessageChange}
                               value={this.state.inputMessage}/>
                    </div>
                    <button onClick={this.handleSubmit} className="btn btn-sm btn-primary btn-block"
                            disabled={this.state.disabledButton}>Envoyer
                    </button>
                </form>
                <p className="text-gray-500" style={{fontSize: '0.9em'}}>&lt;br&gt; peut être utilisé pour sauter une
                    ligne</p>
            </Block>
        );
    }

    handleMessageChange = (e) => {
        this.setState({inputMessage: e.target.value});
    }
}

export default HotelAlert;