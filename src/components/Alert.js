const Alert = (props) => <div className="row" style={{padding: "0 0 0 23px"}}>
    <div className={"alert alert-" + props.color + " alert-dismissible fade show"}
         role="alert">
        {props.children}
        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>;

export default Alert;