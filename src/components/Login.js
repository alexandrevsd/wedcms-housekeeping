import React from 'react';
import Container from './login/Container';
import Form from './login/Form';
import Input from './login/Input';
import SubmitButton from './login/SubmitButton';
import {api} from "../API";

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inputUsername: '',
            inputPassword: '',
            buttonDisabled: false
        }
    }

    handleFormSubmit = async (e) => {
        e.preventDefault();
        this.setState({buttonDisabled: true});
        const response = await api.post('/api/auth/login', {username: this.state.inputUsername, password: this.state.inputPassword})
        const res = response.data;
        if(res.user) {
            this.props.app.setLogged(res.user);
        } else {

        }
        this.setState({buttonDisabled: false});
    }

    render() {
        return (
            <Container>
                <Form title={'Connecte-toi !'}>
                    <Input name={'username'} type={'text'} placeholder={'Pseudo Habbo'} value={this.state.inputUsername} onChange={this.handleInputUsernameChange}/>
                    <Input name={'password'} type={'password'} placeholder={'Mot de passe'} value={this.state.inputPassword} onChange={this.handleInputPasswordChange}/>
                    <SubmitButton disabled={this.state.buttonDisabled} text={'Se connecter'} submitForm={this.handleFormSubmit}/>
                </Form>
            </Container>
        );
    }

    handleInputUsernameChange = (e) => {
        this.setState({inputUsername: e.target.value});
    }

    handleInputPasswordChange = (e) => {
        this.setState({inputPassword: e.target.value});
    }

}

export default Login;